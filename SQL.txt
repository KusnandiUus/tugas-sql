1. Buat database
	CREATE DATABASE myshop;

2. Buat tabel 
	# tabel user
		CREATE TABLE user( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, email varchar(255) NOT null, PASSWORD varchar(255) NOT null );

	# tabel categories
		CREATE TABLE categories( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null );

	# tabel items
		CREATE TABLE items( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255) NOT null, price int NOT null, stock int NOT null, category_id int NOT null, FOREIGN KEY(category_id) REFERENCES categories(id) );

3. memasukkan data pada tabel
	# tabel user
		INSERT INTO user(name, email, PASSWORD) VALUES("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");
		
	# tabel categories
		INSERT INTO categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded");
		
	# tabel items
		INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang B50", "Hape keren dadri merek Sumsang", 4000000, 100, 1), ("Uniklooh", "Baju keren dari merek ternama", 500000, 50, 2), ("IMHO Watch", "Jam tangan ank yang jujur banget", 2000000, 10, 1);

4. mengambil data 
	a. mengambil data user kecuali password
		SELECT id, name, email FROM user;
		
	b. mengambil data dari item
		# cari item dengan harga diatas 1000000
			SELECT * FROM `items` WHERE price > 1000000;
			
		# mengambil data dari item yang mirip atau serupa
			SELECT * FROM `items` WHERE name like "%sang%";
			
	c. menampilkan data tampilan items join dengan kategori
			SELECT items.name, items.description, items.price, items.stock, categories.id, categories.name FROM items INNER JOIN categories ON items.category_id = categories.id;
			
5. mengubah data dari database
	UPDATE items SET price = 2500000 WHERE id=1;

		